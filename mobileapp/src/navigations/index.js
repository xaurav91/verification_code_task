import React, {useContext} from 'react';
import VerificationScreen from '../screens/verification.screen';
import {VerificationContext} from '../context/verification.context';
import HomeScreen from '../screens/home.screen';

const Navigation = () => {
  const verificationContext = useContext(VerificationContext);

  if (verificationContext.state.success) {
    return <HomeScreen />;
  } else {
    return <VerificationScreen />;
  }
};

export default Navigation;
