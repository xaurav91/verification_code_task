import React from 'react';
import Navigation from './navigations';
import {SafeAreaView, StatusBar} from 'react-native';
import {VerificationProvider} from './context/verification.provider';

const App = () => {
  return (
    <SafeAreaView style={{flex: 1}}>
      <StatusBar barStyle="dark-content" backgroundColor="transparent" />
      <VerificationProvider>
        <Navigation />
      </VerificationProvider>
    </SafeAreaView>
  );
};

export default App;
