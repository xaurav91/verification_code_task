import React, {useReducer} from 'react';
import {VerificationContext} from './verification.context';
import {verificationReducer} from './verification.reducer';

export function VerificationProvider({children}) {
  const [state, dispatch] = useReducer(verificationReducer, {});

  return (
    <VerificationContext.Provider value={{state, dispatch}}>
      {children}
    </VerificationContext.Provider>
  );
}
