import VerificationService from '../services/verification.service';

export const verificationAction = {
  verify,
};

function verify({dispatch, code}) {
  dispatch({type: 'FETCHING'});
  VerificationService.verify({code}).then(
    data => {
      dispatch({type: 'FETCHED', data});
    },
    error => {
      dispatch({type: 'ERROR', error});
    },
  );
}
