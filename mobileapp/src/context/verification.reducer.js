export const verificationReducer = (state = {}, action) => {
  switch (action.type) {
    case 'FETCHING':
      return {loading: true};
    case 'FETCHED':
      return {success: action.data.success, message: action.data.message};
    case 'ERROR':
      return {success: false, error: action.error.message};
    default:
      return state;
  }
};
