class InputService {
  getInputSate(state, value) {
    state.value = value;
    state.hasError = !this._isNumber(value);
    return state;
  }

  _isNumber(value) {
    const regex = new RegExp(/^\d+$/);
    return regex.test(value);
  }

  parseInput(oldInput = '', newInput = '') {
    return newInput.replace(oldInput, '').split('');
  }

  clearState() {
    return {value: '', hasError: false};
  }

  getStateSnapshot(state) {
    return JSON.parse(JSON.stringify(state));
  }

  validateAllInputs = state => {
    let hasError = false;
    for (let input of state) {
      input.hasError = !input.value || !this._isNumber(input.value);
      if (input.hasError) {
        hasError = true;
      }
    }
    return {state, hasError};
  };
}

export default new InputService();
