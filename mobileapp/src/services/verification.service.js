import {API_URL} from '@env';

class VerificationService {
  verify(data) {
    if (!API_URL) {
      console.log('API_URL is not present on .env file using default');
    }
    // use default URL if api url is not present
    const url = `${
      API_URL || 'https://verification-node.herokuapp.com'
    }/verify`;
    return fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application-json',
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: JSON.stringify(data),
    }).then(this.handleResponse);
  }

  handleResponse(response) {
    return response.text().then(text => {
      const data = (text && JSON.parse(text)) || {};
      if (!response.ok) {
        return Promise.reject({
          message: data.message || 'Some thing went wrong.',
        });
      }

      return data;
    });
  }
}

export default new VerificationService();
