import React from 'react';
import {StyleSheet, Text} from 'react-native';

const Heading = ({title}) => {
  return <Text style={styles.heading}>{title}</Text>;
};
const styles = StyleSheet.create({
  heading: {
    fontSize: 20,
    color: '#333333',
    fontWeight: 'bold',
    lineHeight: 30,
  },
});

export default Heading;
