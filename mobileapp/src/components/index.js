export {default as Heading} from './heading.component';
export {default as Button} from './button.component';
export {default as Container} from './container.component';
export {default as InputBox} from './input-box.component';
export {default as InputContainer} from './input-container.component';
export {default as Alert} from './alert.component';
