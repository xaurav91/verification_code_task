import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const Alert = ({message}) => {
  return (
    <View>
      <Text style={styles.error}>{message}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  error: {
    color: 'red',
    fontSize: 15,
  },
});
export default Alert;
