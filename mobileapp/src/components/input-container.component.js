import React from 'react';
import {StyleSheet, View} from 'react-native';

const InputContainerComponent = ({children}) => {
  return <View style={styles.inputContainer}>{children}</View>;
};
const styles = StyleSheet.create({
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 20,
  },
});

export default InputContainerComponent;
