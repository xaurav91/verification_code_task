import React, {forwardRef} from 'react';
import {StyleSheet, TextInput} from 'react-native';

const InputBox = forwardRef(
  (
    {hasError = false, value = '', onChangeText, onKeyPress, selectTextOnFocus, autoFocus = false},
    ref,
  ) => {
    return (
      <TextInput
        ref={ref}
        style={[styles.input, hasError && styles.inputError]}
        value={value}
        keyboardType={'number-pad'}
        onChangeText={onChangeText}
        onKeyPress={onKeyPress}
        selectTextOnFocus={selectTextOnFocus}
        autoFocus={autoFocus}
      />
    );
  },
);

const styles = StyleSheet.create({
  input: {
    width: 38,
    height: 38,
    borderWidth: 1.2,
    borderColor: '#333333',
    borderRadius: 5,
    margin: 4,
    fontSize: 20,
    color: '#333333',
    padding: 0,
    textAlign: 'center',
  },
  inputError: {
    borderColor: 'red',
  },
});

export default InputBox;
