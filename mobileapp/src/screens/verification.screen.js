import React, {createRef, useContext, useEffect, useRef, useState} from 'react';
import {
  Alert,
  Button,
  Container,
  Heading,
  InputBox,
  InputContainer,
} from '../components';
import TextInputService from '../services/input.service';
import {VerificationContext} from '../context/verification.context';
import {verificationAction} from '../context/verification.action';
import {ActivityIndicator} from 'react-native';

const MAX_INPUT = 6;
const INPUT_ARRAY = [...Array(MAX_INPUT).keys()];

const VerificationScreen = () => {
  // context
  const verificationContext = useContext(VerificationContext);

  // initialize state
  const [inputStates, setInputStates] = useState(
    INPUT_ARRAY.map(() => ({hasError: false, value: ''})),
  );
  const [error, setError] = useState('');

  // set error from api
  useEffect(() => {
    if (!error && verificationContext.state.error) {
      setError(verificationContext.state.error);
    }
  }, [verificationContext]);

  // create input refs
  const inputRefs = useRef(INPUT_ARRAY.map(() => createRef()));

  // handle input change
  const handleTextChange = (index, text) => {
    const state = TextInputService.getStateSnapshot(inputStates);
    const inputArr = TextInputService.parseInput(state[index].value, text);

    let nextIndex = index;
    for (let i = 0; i < inputArr.length; i++) {
      if (MAX_INPUT <= nextIndex) {
        break;
      }
      if (inputRefs.current[nextIndex + 1]) {
        inputRefs.current[nextIndex + 1].current.focus();
      }
      state[nextIndex] = TextInputService.getInputSate(
        state[nextIndex],
        inputArr[i],
      );
      nextIndex++;
    }
    const hasError = state.find(item => item.hasError);
    setError(hasError ? 'Invalid Input' : '');
    setInputStates(state);
  };

  // handle Enter or backspace press
  const handleKeyPress = (i, eventType) => {
    if (eventType === 'Backspace') {
      const state = TextInputService.getStateSnapshot(inputStates);

      if (inputRefs.current[i - 1]) {
        inputRefs.current[i - 1].current.focus();
      }
      state[i] = TextInputService.clearState();
      setInputStates(state);
    }
  };

  const handleSubmit = () => {
    if (error) {
      return;
    }
    const inputState = TextInputService.getStateSnapshot(inputStates);
    const {hasError, state} = TextInputService.validateAllInputs(inputState);
    setInputStates(state);
    if (hasError) {
      setError('Invalid Input');
      return;
    }

    const inputCode = state
      .map(field => field.value)
      .filter(d => d)
      .join('');
    verificationAction.verify({
      dispatch: verificationContext.dispatch,
      code: inputCode,
    });
  };

  return (
    <Container>
      <Alert message={error} />
      <Heading title={'Verification Code:'} />
      <InputContainer>
        {inputStates.map((state, i) => (
          <InputBox
            ref={inputRefs.current[i]}
            key={i}
            selectTextOnFocus={true}
            onChangeText={text => handleTextChange(i, text)}
            onKeyPress={e => handleKeyPress(i, e.nativeEvent.key)}
            autoFocus={i === 0}
            {...state}
          />
        ))}
      </InputContainer>
      {!verificationContext.state.loading ? (
        <Button title={'SUBMIT'} onPress={() => handleSubmit()} />
      ) : (
        <ActivityIndicator size="large" />
      )}
    </Container>
  );
};

export default VerificationScreen;
