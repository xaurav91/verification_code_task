import React from 'react';
import {Text} from 'react-native';
import Container from '../components/container.component';
import {Heading} from '../components';

const HomeScreen = () => {
  return (
    <Container>
      <Heading title={'Welcome'} />
      <Text>Verification was success</Text>
    </Container>
  );
};

export default HomeScreen;
