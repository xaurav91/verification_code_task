# Verification Code Task
This repository contains Nodejs server and React Native mobile app.

### Getting Started
#### Server
- cd to `server`
- Install the dependency using `yarn install` or `npm install`
- Create `.env` file on the root directory and set `PORT` (check `.env.example' for reference)
- Run the server using `node app.js` command

#### Mobile App
- cd to `mobileapp`
- Install the dependency using `yarn install` or `npm install`
- Create `.env` file on the root directory and set `API_URL` (check `.env.example` for reference)
Note: if `API_URL` is not provided it will default to `https://verification-node.herokuapp.com`
- Run the app using following commands:
```bash
 yarn android (for android devices)
 yarn ios (for ios devices)
```

### Features
- User can manually enter digits and copy from clipboard as well.
- Redirects to success page if verification is success
- If any of the field is invalid, the field is highlighted
- Server sends "Verification Error" message if last digit is 7
- Both client side and server side validation

### Notes
- I have added apk and video demo of the mobile app with this repository
- The server is also hosted at: https://verification-node.herokuapp.com
