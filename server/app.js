import express from 'express';
import dotenv from 'dotenv';
import cors from 'cors';
import VerificationController from "./controller/verification.js";
import errorHandler from "./utils/errorHandler.js";
import handleNotFound from "./utils/handleNotFound.js";

// process .env file
dotenv.config()

const app = express()
const port = process.env.PORT;

// cors middleware
app.use(cors())

// json middleware
app.use(express.json())

// verify route
app.post('/verify', (req, res, next) => VerificationController.verify(req, res, next));

// handle 404 pages
app.use((req, res, next) => handleNotFound(req, res, next));

// handle app error
app.use((err, req, res, next) => errorHandler(err, req, res, next));


app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`)
})
