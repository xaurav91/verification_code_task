import {writeJsonResponse} from "../utils/writeJsonResponse.js";
import VerificationService from "../service/verification.js";
import {ValidationException} from "../exceptions/httpException.js";

class VerificationController {
    verify(req, res, next) {
        try {
            const { code } = req.body;
            // validate user input
            const isValid = VerificationService.validate(code);
            if (!isValid) {
                throw new ValidationException('Invalid input');
            }


            // verify code and send response
            writeJsonResponse(res, 200, {
                success: true,
                message: VerificationService.verify(code)
            });
        } catch (err) {
            next(err);
        }
    }
}

export default new VerificationController();
