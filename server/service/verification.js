import {BadRequestException} from "../exceptions/httpException.js";

class VerificationService {
    validate(code) {
        return (code && code.length === 6 && this._isNumber(code));
    }

    verify(code) {
         if (code.endsWith('7')) {
             throw new BadRequestException('Verification Error');
         }
         return code;
    }

    _isNumber(value) {
        const regex = new RegExp(/^\d+$/);
        return regex.test(value);
    }
}

export default new VerificationService();
