import {writeJsonResponse} from "./writeJsonResponse.js";

const handleNotFound = (req, res, next) => {
    writeJsonResponse(res, 404, {
        success: false,
        message: 'Not found'
    });
};

export default handleNotFound;
