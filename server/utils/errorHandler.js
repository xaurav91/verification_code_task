import {HttpException, ValidationException} from "../exceptions/httpException.js";
import {writeJsonResponse} from "./writeJsonResponse.js";

const errorHandler = (error, req, res, next) => {
    if (error instanceof ValidationException) {
        return writeJsonResponse(res, error.getHttpStatusCode(), error.getResponse());
    }

    if (error instanceof HttpException) {
        return writeJsonResponse(res, error.getHttpStatusCode(), error.getResponse());
    }

    // log error
    console.log(JSON.stringify({message: getMessage(error, req)}, null, 2), error);

    return writeJsonResponse(res, 500, {message: 'Server Error'});
};

const getMessage = (error, req) => {
    const {ip, hostname, originalUrl} = req;
    return {text: error.message, ip, url: hostname + originalUrl}
};

export default errorHandler;
