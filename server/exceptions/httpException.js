export class HttpException extends Error {
    constructor(message, httpStatusCode, response) {
        super(message);
        this.httpStatusCode = httpStatusCode || 500;
        this.response = response;
        Object.setPrototypeOf(this, HttpException.prototype);
    }

    getHttpStatusCode() {
        return this.httpStatusCode;
    }

    getResponse() {
        return {
            success: false,
            message: this.message || this.response || ''
        };
    }
}

export class BadRequestException extends HttpException {
    constructor(message, response) {
        super(message, 400, response);
        Object.setPrototypeOf(this, BadRequestException.prototype);
    }
}

export class ValidationException extends HttpException {
    constructor(message, response) {
        super(message, 422, response);
        Object.setPrototypeOf(this, ValidationException.prototype);
    }
}

export class NotFoundException extends HttpException {
    constructor(message) {
        super(message, 404, 'Not Found');
        Object.setPrototypeOf(this, NotFoundException.prototype);
    }
}
